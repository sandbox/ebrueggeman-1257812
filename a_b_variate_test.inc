<?php

// this file is loaded as a custom session_inc, so init sesstion as usual here
require_once variable_get('original_session_inc', './includes/session.inc');

ABVariateTest::check();

class ABVariateTest {

  private static $_orig_path;
  private static $_used_path;
  private function __construct() {}
  
  public static function check() {
    //use Drupal's request_uri() method to get page requested
    $req = self::variate_request_uri();
	
	self::$_orig_path = $req;
	
    //assess path eligibility see if a matching version is in the variate cache
    if ($variate_cache = cache_get('a_b_variate_cache_paths', 'cache')) {
      if ($variate_cache->data && is_array($variate_cache->data) && isset($variate_cache->data[$req])) {
 
        $variate_array = $variate_cache->data[$req];
 
        $already_seen = FALSE;
        $variates_seen = NULL;
        $percent = $variate_array['percent'];
        $variate = $variate_array['path'];
        $ab_id = $variate_array['ab_id'];

        //check for value of existing seen page variate
        if (isset($_COOKIE['a_b_variate']) && $variates_seen = $_COOKIE['a_b_variate']) {
          $variates_seen = unserialize($variates_seen);

          if (isset($variates_seen[$req]) && 
            ($path_seen = $variates_seen[$req]['path']) && 
            ($ab_id_seen = $variates_seen[$req]['ab_id'])) {
  
            //this user has seen a current version of this variate test
            if ($ab_id_seen == $ab_id) {
              self::alter_path($path_seen);
              $already_seen = TRUE;
            }
            else {
              //this user's cookie has an outdated ab_id, remove from cookie
              unset($variates_seen[$req]);
            }
          }
        }
  
        //check to see if "b" variate should be shown
        if (!$already_seen && $percent <= mt_rand(1,100)) {
          self::alter_path($variate);
        }
        //else: show user page as requested

        //set or update cookie with variates seen information
        $variates_seen = isset($variates_seen) ? $variates_seen : array();
        $variates_seen[$req] = array('path' => self::variate_request_uri(), 'ab_id' => $ab_id);
        setcookie('a_b_variate', serialize($variates_seen), time()+60*60*24*30, '/', variable_get('cookie_domain', NULL));
      }
    }
	//save variate path for later
	self::$_used_path = self::variate_request_uri();
  }
  
  protected static function alter_path($new_path) {
    $new_path = '/' . ltrim($new_path, '/');
    //request_uri() used later in the Drupal bootstrap checks these vars
    $_SERVER['REQUEST_URI'] =  $new_path;
    $_SERVER['SCRIPT_NAME'] = $new_path;
    $_GET['q'] = $new_path;
  }
  
  protected static function variate_request_uri() {
	$req = ltrim(request_uri(), '/');
	//strip query args from request
	$req_parts = explode('?', $req);
	return $req_parts[0];
  }
  
  public static function is_variate() {
    return (self::$_used_path != self::$_orig_path) ? TRUE : FALSE;
  }
  
  public static function get_orig_path($name) { return self::$_orig_path; }
  
  public static function get_used_path($name) { return self::$_used_path; }

}

